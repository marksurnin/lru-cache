package com.cache.lru;


import com.cache.Cache;

import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.PreconditionViolationException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LRUCacheTest {

    @Test
    public void testCacheWithZeroSize() {
        final Cache<Integer, Integer> cache = new LRUCache<>(0);
        assertEquals(0, cache.size());
    }

    @Test
    public void testCacheWithNegativeSizeFails() {
        assertThrows(PreconditionViolationException.class, () -> {
            new LRUCache<>(-1);
        });
    }

    @Test
    public void testCachePutOneElement() {
        final Cache<Integer, Integer> cache = new LRUCache<>(1);
        cache.put(1, 2);
        assertEquals(1, cache.size());
        assertEquals(Optional.of(2), cache.get(1));
    }

    @Test
    public void testCachePutAndUpdateOneElement() {
        final Cache<Integer, Integer> cache = new LRUCache<>(1);
        cache.put(1, 2);
        cache.put(1, 20);
        assertEquals(1, cache.size());
        assertEquals(Optional.of(20), cache.get(1));
    }

    @Test
    public void testCacheWithCharacterAndStringTypes() {
        final Cache<Character, String> cache = new LRUCache<>(2);
        final char key1 = 'a';
        final char key2 = 'b';
        final String value1 = "string A";
        final String value2 = "string B";

        cache.put(key1, value1);
        cache.put(key2, value2);

        assertEquals(2, cache.size());
        assertEquals(Optional.of(value1), cache.get(key1));
        assertEquals(Optional.of(value2), cache.get(key2));
    }

    @Test
    public void testCacheWithIntegerAndCustomTypes() {
        class Helper {
            int val;
            Helper(int val) {
                this.val = val;
            }
        }

        final Cache<Integer, Helper> cache = new LRUCache<>(2);
        final int key1 = 1;
        final int key2 = 2;
        final Helper value1 = new Helper(100);
        final Helper value2 = new Helper(200);

        cache.put(key1, value1);
        cache.put(key2, value2);

        assertEquals(2, cache.size());
        assertTrue(cache.get(key1).isPresent());
        assertTrue(cache.get(key2).isPresent());
        assertEquals(100, cache.get(key1).get().val);
        assertEquals(200, cache.get(key2).get().val);
    }

    @Test
    public void testCachePutMultipleElementsDoesNotExceedCapacity() {
        final Cache<Integer, Integer> cache = new LRUCache<>(10);
        cache.put(1, 2);
        cache.put(-10, -20);
        cache.put(100, 200);
        cache.put(-1000, -2000);
        cache.put(10000, 20000);

        assertEquals(5, cache.size());
        assertEquals(Optional.of(2), cache.get(1));
        assertEquals(Optional.of(-20), cache.get(-10));
        assertEquals(Optional.of(200), cache.get(100));
        assertEquals(Optional.of(-2000), cache.get(-1000));
        assertEquals(Optional.of(20000), cache.get(10000));
    }

    @Test
    public void testCachePutMultipleElementsExceedsCapacity() {
        final Cache<Integer, Integer> cache = new LRUCache<>(3);
        cache.put(1, 2);
        cache.put(-10, -20);
        cache.put(100, 200);
        cache.put(-1000, -2000);

        assertEquals(3, cache.size());
        assertFalse(cache.get(1).isPresent());
        assertEquals(Optional.of(-20), cache.get(-10));
        assertEquals(Optional.of(200), cache.get(100));
        assertEquals(Optional.of(-2000), cache.get(-1000));
    }

    @Test
    public void testCachePutAndUpdateMultipleElementsExceedsCapacity() {
        final Cache<String, String> cache = new LRUCache<>(2);
        cache.put("one", "one");
        cache.put("two", "two");
        assertEquals(Optional.of("one"), cache.get("one"));

        // "two" is evicted
        cache.put("three", "three");
        assertFalse(cache.get("two").isPresent());

        // "one" is evicted
        cache.put("four", "four");
        assertFalse(cache.get("one").isPresent());

        assertEquals(Optional.of("four"), cache.get("four"));
        assertEquals(Optional.of("three"), cache.get("three"));
    }

    @Test
    public void testCacheExceedsAccessLimit() {
        final Cache<Integer, Integer> cache = new LRUCache<>(1);
        cache.put(1, 2);

        final int accessLimit = ((LRUCache) cache).getAccessLimit();
        for (int i = 0; i < accessLimit; i++) {
            assertEquals(Optional.of(2), cache.get(1));
        }

        assertFalse(cache.get(1).isPresent());
        assertTrue(cache.isEmpty());
    }

    @Test
    public void testCacheExceedsAccessLimitWithSubsequentPut() {
        final Cache<Integer, Integer> cache = new LRUCache<>(1);
        cache.put(1, 2);

        final int accessLimit = ((LRUCache) cache).getAccessLimit();
        for (int i = 0; i < accessLimit; i++) {
            cache.get(1);
        }

        assertFalse(cache.get(1).isPresent());
        cache.put(1, 3);
        assertEquals(Optional.of(3), cache.get(1));
    }

    @Test
    public void testCacheWithMultipleGetAndPutsExceedingAccessLimit() {
        final Cache<String, String> cache = new LRUCache<>(2);
        final int accessLimit = ((LRUCache) cache).getAccessLimit();

        cache.put("one", "one");
        cache.put("two", "two");
        for (int i = 0; i < accessLimit; i++) {
            assertEquals(Optional.of("one"), cache.get("one"));
        }

        // "one" is removed and cache size has not reached capacity
        assertFalse(cache.get("one").isPresent());
        assertEquals(1, cache.size());
        cache.put("three", "three");

        // "one" is evicted
        assertFalse(cache.get("one").isPresent());

        for (int i = 0; i < accessLimit; i++) {
            assertEquals(Optional.of("two"), cache.get("two"));
            assertEquals(Optional.of("three"), cache.get("three"));
        }
        assertTrue(cache.isEmpty());
    }
}