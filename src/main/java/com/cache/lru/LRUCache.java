package com.cache.lru;

import com.cache.Cache;
import net.jcip.annotations.NotThreadSafe;
import org.junit.platform.commons.util.Preconditions;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


/**
 * This is an implementation of a cache whose entries are evicted based on
 * two conditions.
 *
 * The first is condition happens when the cache size exceeds
 * its capacity, and entries are evicted according to the least recently
 * used policy. The second condition happens when a given cache entry
 * has been accessed more times than the read access limit.
 *
 * The least recently used policy is implemented using a linked list and a map.
 * The values of the linked list contain cache keys with type K. The node that
 * follows the head node is most recently used and the node that precedes
 * the tail node is least frequently used.
 *
 * These keys are used to access cache entries in a map, which store the value,
 * the access counter, and a reference to the linked list node.
 *
 * The cache supports generic type parameters. The implementation is not thread
 * safe.
 *
 * @param <K> the type of keys maintained by this cache
 * @param <V> the type of mapped values
 *
 * @author  Mark Surnin
 */
public class LRUCache<K, V> implements Cache<K, V> {
    private final static int ACCESS_LIMIT = 10;
    private final int capacity;
    final Map<K, LRUCacheEntry<V, K>> map;
    final LinkedListNode<Integer> head;
    final LinkedListNode<Integer> tail;

    public LRUCache(final int capacity) {
        Preconditions.condition(capacity >= 0, "Capacity must be non-negative.");
        this.capacity = capacity;

        map = new HashMap<>();

        // Even though the type parameter of the head and tail nodes is Integer,
        // they can be linked to nodes with any type parameter.
        head = new LinkedListNode<>(Integer.MAX_VALUE);
        tail = new LinkedListNode<>(Integer.MAX_VALUE);
        head.setNext(tail);
        tail.setPrev(head);
    }

    /**
     * If there is no cache entry with the given `key`, a new cache entry
     * with value `value` is created. If there is a cache entry with
     * the given `key`, the value is updated to `value`.
     *
     * The linked list node with key `key` is marked as most recently used
     * by creating it at (or moving to) the second position in the list.
     *
     * @param key cache key
     * @param value cache value
     */
    @Override
    public void put(final K key, final V value) {
        if (map.containsKey(key)) {
            final LRUCacheEntry<V, K> cacheEntry = map.get(key);
            cacheEntry.setValue(value);

            // Move the node to the beginning of the list (after head).
            final LinkedListNode<K> keyNode = cacheEntry.getKeyNode();
            removeNode(keyNode);
            makeNodeLRU(keyNode);
        } else {
            final LinkedListNode<K> node = new LinkedListNode<>(key);
            final LRUCacheEntry<V, K> cacheEntry = new LRUCacheEntry<>(value, node);
            map.put(key, cacheEntry);

            // Store the node at the beginning of the list (after head).
            makeNodeLRU(node);
        }

        if (map.size() > capacity) {
            removeMostRecentlyUsedCacheEntry();
        }
    }

    /**
     * Removes the most recently used cache entry by removing the node in the
     * list that precedes the tail node and by removing the cache entry with
     * the key that the removed node contained.
     */
    private void removeMostRecentlyUsedCacheEntry() {
        if (this.size() == 0 || tail.getPrev().equals(head)) {
            return;
        }

        final LinkedListNode<K> mruNode = tail.getPrev();
        removeNode(mruNode);
        final K cacheKey = mruNode.getValue();
        map.remove(cacheKey);
    }

    /**
     * Returns the value that a given `key` is mapped to, if it exists.
     * The entry is marked as most frequently used and the access counter is
     * incremented.
     *
     * If the access counter is greater than or equal to `ACCESS_LIMIT`,
     * the key node and the associated cache entry are removed.
     *
     * @param key
     * @return an Optional containing the value or an empty Optional.
     */
    @Override
    public Optional<V> get(final K key) {
        if (!map.containsKey(key)) {
            return Optional.empty();
        }

        final LRUCacheEntry<V, K> entry = map.get(key);
        entry.incrementAccessCounter();

        // Move the key node to the beginning of the list (after head).
        final LinkedListNode<K> cacheNode = entry.getKeyNode();
        removeNode(cacheNode);
        makeNodeLRU(cacheNode);

        // move the node to the head
        if (entry.getAccessCounter() >= ACCESS_LIMIT) {
            removeNode(cacheNode);
            map.remove(key);
        }
        return Optional.of(entry.getValue() );
    }

    /**
     * Removes the linked list node.
     * @param cacheNode
     */
    private void removeNode(final LinkedListNode<K> cacheNode) {
        cacheNode.getPrev().setNext(cacheNode.getNext());
        cacheNode.getNext().setPrev(cacheNode.getPrev());
    }

    /**
     * Moves a given node to the beginning of the list (after head).
     * @param cacheNode
     */
    private void makeNodeLRU(final LinkedListNode<K> cacheNode) {
        cacheNode.setNext(head.getNext());
        cacheNode.getNext().setPrev(cacheNode);
        cacheNode.setPrev(head);
        head.setNext(cacheNode);
    }

    @Override
    public int size() {
        return map.size();
    }

    public int getAccessLimit() {
        return ACCESS_LIMIT;
    }
}
