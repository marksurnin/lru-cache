package com.cache.lru;

/**
 * A helper container for the cache entry, which stores the value,
 * the key access counter, and a reference to the linked list node.
 */
class LRUCacheEntry<V, K> {
    private int accessCounter;
    private LinkedListNode<K> keyNode;
    private V value;

    LRUCacheEntry(final V value, final LinkedListNode<K> keyNode) {
        this.value = value;
        this.accessCounter = 0;
        this.keyNode = keyNode;
    }

    LinkedListNode<K> getKeyNode() {
        return keyNode;
    }

    void incrementAccessCounter() {
        accessCounter++;
    }

    int getAccessCounter() {
        return accessCounter;
    }

    V getValue() {
        return value;
    }

    void setValue(V value) {
        this.value = value;
    }
}
