package com.cache.lru;

class LinkedListNode<K> {
    private final K value;

    // `next` and `prev` are not parameterized so that head and tail nodes
    // that have type parameter <Integer> can be linked to nodes with
    // another type parameter.
    private LinkedListNode next;
    private LinkedListNode prev;

    LinkedListNode(final K value) {
        this.value = value;
    }

    K getValue() {
        return this.value;
    }

    void setNext(final LinkedListNode next) {
        this.next = next;
    }

    LinkedListNode getNext() {
        return this.next;
    }

    void setPrev(final LinkedListNode prev) {
        this.prev = prev;
    }

    LinkedListNode getPrev() {
        return this.prev;
    }
}
