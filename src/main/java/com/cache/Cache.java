package com.cache;

import java.util.Optional;

public interface Cache<K, V> {
    void put(final K key, final V value);
    Optional<V> get(final K key);
    int size();

    default boolean isEmpty() {
        return size() == 0;
    }
}