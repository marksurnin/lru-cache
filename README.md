This is an implementation of a cache whose entries are evicted based on two conditions.

The first is condition happens when the cache size exceeds its capacity, and entries are evicted according to the least recently used policy. The second condition happens when a given cache entry has been accessed more times than the read access limit.

The least recently used policy is implemented using a linked list and a map. The values of the linked list contain cache keys with type K. The node that follows the head node is most recently used and the node that precedes the tail node is least frequently used.

These keys are used to access cache entries in a map, which store the value, the access counter, and a reference to the linked list node.

The cache supports generic type parameters. The implementation is not thread safe.